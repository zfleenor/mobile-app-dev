package com.example.fleenor.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebSettings;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView myWebView = (WebView) findViewById(R.id.webview);

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        setContentView(myWebView);

        String summary = "// Docs at http://simpleweatherjs.com\n" +
                "$(document).ready(function() {\n" +
                "  $.simpleWeather({\n" +
                "    woeid: '2357536', //2357536\n" +
                "    location: '',\n" +
                "    unit: 'f',\n" +
                "    success: function(weather) {\n" +
                "      html = '<h2>'+weather.temp+'&deg;'+weather.units.temp+'</h2>';\n" +
                "      html += '<ul><li>'+weather.city+', '+weather.region+'</li>';\n" +
                "      html += '<li class=\"currently\">'+weather.currently+'</li>';\n" +
                "      html += '<li>'+weather.alt.temp+'&deg;C</li></ul>';\n" +
                "      \n" +
                "      for(var i=0;i<weather.forecast.length;i++) {\n" +
                "        html += '<p>'+weather.forecast[i].day+': '+weather.forecast[i].high+'</p>';\n" +
                "      }\n" +
                "  \n" +
                "      $(\"#weather\").html(html);\n" +
                "    },\n" +
                "    error: function(error) {\n" +
                "      $(\"#weather\").html('<p>'+error+'</p>');\n" +
                "    }\n" +
                "  });\n" +
                "});\n";

        myWebView.loadData(summary, "text/js", null);

    }
}
