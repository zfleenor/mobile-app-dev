package com.example.fleenor.myvoipphone;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;




public class MainActivity extends AppCompatActivity {

    StringBuilder num_to_call = new StringBuilder();
    private ButtonClickListener mClickListener;

    public void client() throws IOException{

        Context context = getApplicationContext();

        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, "Start", duration);
        toast.show();

        DatagramSocket client_socket = new DatagramSocket(3010);
        InetAddress IPAddress =  InetAddress.getByName("255.255.255.255");

        String str = "poop";

        byte[] send_data = str.getBytes();

        DatagramPacket send_packet = new DatagramPacket(send_data, str.length(), IPAddress, 3010);

        client_socket.send(send_packet);
        Toast toast2 = Toast.makeText(context, "Sent", duration);
        toast.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                try {
                    client();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });


        mClickListener = new ButtonClickListener();

        int idList[] = {
                R.id.button_zero, R.id.button_one, R.id.button_two,
                R.id.button_three, R.id.button_four, R.id.button_five, R.id.button_six,
                R.id.button_seven, R.id.button_eight, R.id.button_nine, R.id.button_zero,
                R.id.button_clear
        };

        for (int id : idList) {
            View v = findViewById(id);
            v.setOnClickListener(mClickListener);
        }
    }






     @Override
        public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }



private class ButtonClickListener implements OnClickListener {

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_clear:
                num_to_call.delete(0,num_to_call.length());
                break;

            default:
                num_to_call.append(((Button) v).getText());

                Context context = getApplicationContext();

                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, num_to_call, duration);
                toast.show();
                break;

        }

    }

}

}

